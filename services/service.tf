################################################
## MODULE QUIZ-MAKER STANDARD
################################################

## IF DEVELOPMENT

module "cloud-run-review" {
  source = "garbetjie/cloud-run/google"
  version = "~> 2"
  name     = var.service_name
  image    = var.image
  location = var.location
  allow_public_access = true
  env = [{ key = "DB_HOST",value = var.database_dns_name },
         { key = "DB_USER",value = var.schema_user },
         { key = "DB_NAME",value = var.schema },
         { key = "ENV",value = var.env },
         { key = "DB_PASS",value = var.schema_pass },
         { key = "SSL_ENABLE",value = "true" },
         { key = "TABLE_PREFIX",value = "pgmkr" },
         { key = "WP_CONTENT_DIR_NAME",value = "box" },
         { key = "GOOGLE_APPLICATION_BUCKET_NAME",value = "responda-development.$${publisher}" },
         { key = "GOOGLE_APPLICATION_BUCKET_PROJECT_ID",value = var.project_name },
         { key = "GOOGLE_APPLICATION_CREDENTIALS",value = "/var/www/html/wpapp/box/keys/development.json" },
         { key = "COMMON_FEATURE_URL ",value = "https://common-staging.etus.digital/api" },
         { key = "API_LEADS_URL ",value = "https://leads-email-entry-iktwuiuyiq-ue.a.run.app/" },
         { key = "DXP_URL",value = "https://assets.etus.digital/dxp-staging/" },
         { key = "CF_TOKEN",value = "GdvbU6nH9x3aXdrNQJs7y8cA9SBFwh5GuRtDHVsg" },
         { key = "SENTRY_DSN",value = "https://22cd5911956f438fa711b2979c68b980@o516495.ingest.sentry.io/5924155" },
         { key = "LAST_VERSION",value = var.sha }
         ]

  vpc_access = { connector = var.vpc , egress = "private-ranges-only" }
  max_instances = 2
  min_instances = 0
  port = 80
}

## IF STAGING

module "cloud-run-staging" {
  source = "garbetjie/cloud-run/google"
  version = "~> 2"

  name     = var.service_name
  image    = var.image
  location = var.location
  allow_public_access = true
  env = [{ key = "DB_HOST",value = var.database_dns_name },
         { key = "DB_USER",value = var.schema_user },
         { key = "DB_NAME",value = var.schema },
         { key = "ENV",value = var.env },
         { key = "DB_PASS",value = var.schema_pass },
         { key = "SSL_ENABLE",value = "true" },
         { key = "TABLE_PREFIX",value = "pgmkr" },
         { key = "WP_CONTENT_DIR_NAME",value = "box" },
         { key = "GOOGLE_APPLICATION_BUCKET_NAME",value = "responda-staging.$${publisher}" },
         { key = "GOOGLE_APPLICATION_BUCKET_PROJECT_ID",value = var.project_name },
         { key = "GOOGLE_APPLICATION_CREDENTIALS",value = "/var/www/html/wpapp/box/keys/staging.json" },
         { key = "DOMAIN", value = var.domain_env },
         { key = "COMMON_FEATURE_URL ",value = "https://common-staging.etus.digital/api" },
         { key = "API_LEADS_URL ",value = "https://leads-email-entry-iktwuiuyiq-ue.a.run.app/" },
         { key = "DXP_URL",value = "https://assets.etus.digital/dxp-staging/" },
         { key = "CF_TOKEN",value = "GdvbU6nH9x3aXdrNQJs7y8cA9SBFwh5GuRtDHVsg" },
         { key = "SENTRY_DSN",value = "https://22cd5911956f438fa711b2979c68b980@o516495.ingest.sentry.io/5924155" },
         { key = "LAST_VERSION",value = var.sha }
         ]
  vpc_access = { connector = var.vpc , egress = "private-ranges-only" }
  max_instances = 2
  min_instances = 0
  port = 80
}

## IF PRODUCTION

module "cloud-run-production" {
  source = "garbetjie/cloud-run/google"
  version = "~> 2"

  name     = var.service_name
  image    = var.image
  location = var.location
  allow_public_access = true
  env = [{ key = "DB_HOST",value = var.database_dns_name },
         { key = "DB_USER",value = var.schema_user },
         { key = "DB_NAME",value = var.schema },
         { key = "ENV",value = var.env },
         { key = "DB_PASS",value = var.schema_pass },
         { key = "SSL_ENABLE",value = "true" },
         { key = "TABLE_PREFIX",value = "pgmkr" },
         { key = "WP_CONTENT_DIR_NAME",value = "box" },
         { key = "GOOGLE_APPLICATION_BUCKET_NAME",value = "responda-.$${publisher}" },
         { key = "GOOGLE_APPLICATION_BUCKET_PROJECT_ID",value = var.project_name },
         { key = "GOOGLE_APPLICATION_CREDENTIALS",value = "/var/www/html/wpapp/box/keys/production.json" },
         { key = "DOMAIN", value = var.domain_env },
         { key = "COMMON_FEATURE_URL ",value = "https://common.etus.digital/api" },
         { key = "API_LEADS_URL ",value = "https://leads-entry.etus.digital/" },
         { key = "DXP_URL",value = "https://assets.etus.digital/dxp/" },
         { key = "CF_TOKEN",value = "GdvbU6nH9x3aXdrNQJs7y8cA9SBFwh5GuRtDHVsg" },
         { key = "SENTRY_DSN",value = "https://22cd5911956f438fa711b2979c68b980@o516495.ingest.sentry.io/5924155" },
         { key = "LAST_VERSION",value = var.sha }
         
         ]

  vpc_access = { connector = var.vpc , egress = "private-ranges-only" }
  max_instances = 2
  min_instances = 0
  port = 80

}

################################################
## VARIABLES
################################################

variable "schema_pass" {
  description = "Schema password"
  type = string
  sensitive = false
}

variable "schema_user" {
  description = "Schema user"
  type = string
}

variable "schema" {
  description = "Schema name"
  type = string
}

variable "database_dns_name" {
  description = "Database DNS Name"
  type = string
}

variable "service_name" {
  description = "Service name"
  type = string
}

variable "location" {
  description = "Location name"
  type = string
}

variable "image" {
  description = "Image name"
  type = string
}

variable "env" {
  description = "Environment"
  type = string
}

variable "sha" {
  description = "Latest version"
  type = string
}

variable "vpc" {
  description = "VPC Connector"
  type = string
}
variable "domain_env" {
  description = "Optional variable for DOMAIN env"
  type        = string
  default = "domain"
}

variable "project_name" {
  description = "Project name"
  type = string
}

################################################
## OUTPUTS
################################################

output "service_name" {
    value = length(module.cloud-run-review) > 0 ? module.cloud-run-review[0].name : length(module.cloud-run-staging) > 0 ? module.cloud-run-staging[0].name : length(module.cloud-run-production) > 0 ? module.cloud-run-production[0].name : null
}

output "project" {
    value = length(module.cloud-run-review) > 0 ? module.cloud-run-review[0].project : length(module.cloud-run-staging) > 0 ? module.cloud-run-staging[0].project : length(module.cloud-run-production) > 0 ? module.cloud-run-production[0].project : null
}

output "service_url" {
    value = length(module.cloud-run-review) > 0 ? module.cloud-run-review[0].url : length(module.cloud-run-staging) > 0 ? module.cloud-run-staging[0].url : length(module.cloud-run-production) > 0 ? module.cloud-run-production[0].url : null
}