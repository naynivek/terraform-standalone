################################################
## PROVIDERS
################################################
terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.67.0"
    }
  }
  backend "gcs" {
    bucket = "terraform-state-brius"
    prefix = "ci-defined"
    }
}
provider "google" {
  project = var.project_name
  region  = var.region
  zone    = var.zone
}
provider "google-beta" {
  project = var.project_name
  region  = var.region
  zone    = var.zone
}

################################################
## MODULES
################################################

module "schemas" {
  source        = "./schemas"
  schema        = var.service_name  #tfvars
  instance_name = var.instance_name #tfvars
}

module "service" {
  source            = "./services"
  schema_pass       = module.schemas.password    #module schema
  schema_user       = module.schemas.username    #module schema
  service_name      = var.service_name           #gitlab-ci
  location          = var.location_name          #tfvars
  image             = var.image_name             #gitlab-ci
  schema            = module.schemas.schema_name #tfvars
  database_dns_name = var.database_dns_name      #tfvars
  env               = var.env                    #tfvars
  sha               = var.sha                    #gitlab-ci
  vpc               = var.vpc                    #tfvars
  project_name      = var.project_name           #tfvars
  domain_env        = var.domain_env             #gitlab-ci
}

################################################
## OUTPUTS
################################################

output "schemas" {
  value = {
    user_credentials = module.schemas
  }
  sensitive = true
}

output "services" {
  value = {
    service_info = module.service
  }
}