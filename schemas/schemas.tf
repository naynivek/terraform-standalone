## Create Schema
resource "google_sql_database" "schema" {
    name = var.schema 
    instance = var.instance_name
}

## Create User
resource "random_id" "password" {
  byte_length = 16
}

resource "google_sql_user" "user" {
  name     = substr("u-${google_sql_database.schema.name}", 0,32)
  instance =  var.instance_name
  host = "%"
  password = random_id.password.id
}