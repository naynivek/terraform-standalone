output "username" {
  description = "username"
  value       = google_sql_user.user.name
}

output "password" {
  description = "password"
  value       = nonsensitive(google_sql_user.user.password)
}

output "schema_name" {
  description = "schema name"
  value       = google_sql_database.schema.name
}