variable "project_name" {
  description = "Project name"
  type        = string
}

variable "region" {
  description = "Region name"
  type        = string
}

variable "zone" {
  description = "Zone name"
  type        = string
}
variable "instance_name" {
  description = "Instance name"
  type        = string
}

variable "service_name" {
  description = "Service name"
  type        = string
}

variable "location_name" {
  description = "Location name"
  type        = string
}

variable "image_name" {
  description = "Image name"
  type        = string
}

variable "database_dns_name" {
  description = "Database DNS Name"
  type        = string
}

variable "env" {
  description = "Environment"
  type        = string
}

variable "sha" {
  description = "Lastest sha version"
  type        = string
}

variable "vpc" {
  description = "VPC Connector"
  type        = string
}
variable "domain_env" {
  description = "Optional variable for DOMAIN env"
  type        = string
  default     = "domain"
}