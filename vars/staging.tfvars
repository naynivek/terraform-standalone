project_name      = "etus-media-development-staging"
region            = "us-east1" 
zone              = "us-east1-b" 
instance_name     = "mysql-stag-1"  
service_name      = "terraform-wp"  
location_name     = "us-east1"   
image_name        = "us-east1-docker.pkg.dev/etus-media-mgmt/plusdin/pagemaker-staging:latest"
database_dns_name = "mysql-1.staging.etus.local"  
env               = "stage"
sha               = "version"
vpc               = "cloud-run-staging"